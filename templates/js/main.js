require.config({
	baseUrl:"js/wordcloud",
    paths: {
    	jquery:'../jquery-1.11.1.min',
    	wCloud:"wordcloud"
    }
});

require(["jquery","wCloud"], function($,w) {
	//ource-out
    var wcld = w;
    var option = {
    	styleType:"tilt",//normal ；vertical tilt（45°）
    	data:[
			{"word":"这里","frq":22,"otherMsg":"tip Messege"},
			{"word":"没有","frq":23,"otherMsg":"tip Messege"},
			{"word":"FCUK!","frq":14,"otherMsg":"tip Messege"},
			{"word":"断网了","frq":43,"otherMsg":"tip Messege"},
			{"word":"错了","frq":25,"otherMsg":"tip Messege"},
			{"word":"出错了！！","frq":14,"otherMsg":"tip Messege"},
			{"word":"404","frq":14,"otherMsg":"tip Messege"},
			{"word":"GFW","frq":28,"otherMsg":"tip Messege"}
			]
    }

    var wd = wcld.init($("#wordcloud")[0]);
    option2 = $.ajax(
    	{
    		type: 'GET',
    		contentType: "application/json; charset=utf-8",
    		url:"hotword.json", 
    		async:true,
    		success:function(msg, status){
    			console.log(msg);
    			 wd = wd.setOption(msg);
    		},
    		error:function(xhr, desc,err){
    			console.log(xhr);
    			console.log("Details:" + desc + "\nError:" + err);
    			  wd = wd.setOption(option);
    		} 
    	});
  
  
  

});
