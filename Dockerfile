
FROM ubuntu

ADD src /home/hotword
RUN sudo apt-get -y install wget
RUN wget https://bootstrap.pypa.io/get-pip.py # 下载pip
RUN sudo apt-get -y install python2.7
RUN  python2.7 get-pip.py
RUN rm get-pip.py
RUN sudo pip install jieba 
RUN sudo pip install feedparser 
RUN sudo pip install web.py
WORKDIR /home/hotword
ENTRYPOINT ["python2.7","/home/hotword/index.py"]
EXPOSE 8080

