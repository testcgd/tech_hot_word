#-*- coding:utf-8 -*-
import web
import getjson
import gethotword
import thread
import time

urls = ("/hotword.json","hotword", \
        "/","index")

dic = {"hehe":3, "ee":4}

render = web.template.render('templates')

class hotword:
   def GET(self):
       return render.hotword()

class index:
    def GET(self):
        return render.index()

def update_json():
    while True:
        print "hot word start update"
        dic_list = gethotword.gethotwordlist()
        file_object = open("./templates/hotword.json","w")
        file_object.write(getjson.get_json(dic_list))
        file_object.close()
        print "hot word update finished"
        time.sleep(3600)


    
if __name__ == "__main__":

    thread.start_new_thread (update_json,())
    app = web.application(urls, globals())
    app.run()
